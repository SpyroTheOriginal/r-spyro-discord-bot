﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace rSpyroDiscordBot {
    public class Metrics {
		public class ServerHourlyData {
			public int characterCount;
			public int postCount;
			public readonly List<ulong> activeUsers = new List<ulong>(); // Users who made a post at least once within the hour (will boil down to a number anyways)
			public readonly List<ulong> activeVCUsers = new List<ulong>(); // Users who are on a VC at least once within the hour (will boil down to a number anyways)
			public readonly List<ulong> activeChannels = new List<ulong>();
			public int onlineUsers;
			public int awayUsers;
			public int busyUsers;
			public int users;
		}
		public readonly Dictionary<DateTime, Dictionary<ulong, ServerHourlyData>> data = new Dictionary<DateTime, Dictionary<ulong, ServerHourlyData>>();
    }
}

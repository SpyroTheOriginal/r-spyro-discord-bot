﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rSpyroDiscordBot.Commands {
	class MetricsCommand : Command {
		public override string baseCommand { get { return "metrics"; } }
		public override string group { get { return "statistics"; } }

		public override CommandFlag settings { get { return CommandFlag.RequireAdministrator; } }

		public override async Task<ExecutionStatus> OnExecute(ExecutionInfo executionInfo, List<Tuple<int, string>> args) {
			Settings.GetSettings(executionInfo.guild, out Settings.ServerSettings serverSettings);

			if (args.Count > 0) {
				bool? toggleState = null;
				if (args.Count > 0 && Constants.toggleOnKeywords.Any(x => x.StartsWith(args[0].Item2, StringComparison.CurrentCultureIgnoreCase)))
					toggleState = true;
				else if (args.Count > 0 && Constants.toggleOffKeywords.Any(x => x.StartsWith(args[0].Item2, StringComparison.CurrentCultureIgnoreCase)))
					toggleState = false;

				if (toggleState.HasValue) {
					Program.TemporaryMessage(executionInfo.channel, $"Metrics will now {(toggleState.Value ? "" : "**not** ")}be collected.");

					serverSettings.metricsEnable = toggleState.Value;

					return ExecutionStatus.ModifiedSettings;
				}
			}
			Program.TemporaryMessage(executionInfo.channel, $"Metrics is {(serverSettings.metricsEnable ? "" : "**not** ")}being collected.");

			return ExecutionStatus.None;
		}
	}
}

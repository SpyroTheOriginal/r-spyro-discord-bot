﻿using Discord;
using Discord.WebSocket;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace rSpyroDiscordBot.Commands {
	public class RoleRequestCommand : Command {
		public override string baseCommand { get { return "rolerequest"; } }
		public override string group { get { return "customization"; } }

		public override CommandFlag settings { get { return CommandFlag.RequireModerator; } }

		public class SetChannelCommand : Command {
			public override string baseCommand { get { return "setchannel"; } }

			public override async Task<ExecutionStatus> OnExecute(ExecutionInfo executionInfo, List<Tuple<int, string>> args) {
				Settings.GetSettings(executionInfo.guild, out Settings.ServerSettings serverSettings);
				Settings.SetChannel(executionInfo, args, ref serverSettings.roleRequestChannelId, "automate role requests");

				await Settings.Save();
				await executionInfo.message.DeleteAsync();

				return ExecutionStatus.None;
			}
		}

		public class AddRoleCommand : Command {
			public override string baseCommand { get { return "add"; } }

			public override async Task<ExecutionStatus> OnExecute(ExecutionInfo executionInfo, List<Tuple<int, string>> args) {
				Settings.GetSettings(executionInfo.guild, out Settings.ServerSettings serverSettings);

				// Get guild information
				IReadOnlyCollection<SocketRole> allRoles = executionInfo.guild.Roles;

				string requestedRoleName = executionInfo.message.Content.Substring(args[0].Item1);
				SocketRole requestedRole = allRoles.FirstOrDefault(x => x.Name.StartsWith(requestedRoleName, StringComparison.CurrentCultureIgnoreCase));

				if (requestedRole != null) {
					if (!serverSettings.requestableRoleIds.Contains(requestedRole.Id)) {
						serverSettings.requestableRoleIds.Add(requestedRole.Id);

						Program.TemporaryMessage(executionInfo.channel, $"**{requestedRole.Name}** is now a requestable role.");

						await Settings.Save();
					} else {
						Program.TemporaryMessage(executionInfo.channel, $"{requestedRole.Name} already is a requestable role.");
					}
				} else {
					Program.TemporaryMessage(executionInfo.channel, $"There is no role called or start with **{requestedRole}**.");
				}

				await executionInfo.message.DeleteAsync();
				return ExecutionStatus.None;
			}
		}

		public class RemoveRoleCommand : Command {
			public override string baseCommand { get { return "remove"; } }

			public override async Task<ExecutionStatus> OnExecute(ExecutionInfo executionInfo, List<Tuple<int, string>> args) {
				Settings.GetSettings(executionInfo.guild, out Settings.ServerSettings serverSettings);

				// Get guild information
				IReadOnlyCollection<SocketRole> allRoles = executionInfo.guild.Roles;

				string requestedRoleName = executionInfo.message.Content.Substring(args[0].Item1);
				SocketRole requestedRole = allRoles.FirstOrDefault(x => x.Name.StartsWith(requestedRoleName, StringComparison.CurrentCultureIgnoreCase));

				if (requestedRole != null) {
					if (serverSettings.requestableRoleIds.Contains(requestedRole.Id)) {
						serverSettings.requestableRoleIds.Remove(requestedRole.Id);

						Program.TemporaryMessage(executionInfo.channel, $"**{requestedRole.Name}** is no longer a requestable role.");

						await Settings.Save();
					} else {
						Program.TemporaryMessage(executionInfo.channel, $"{requestedRole.Name} is not a cosmetic role.");
					}
				} else {
					Program.TemporaryMessage(executionInfo.channel, $"There is no role called or start with **{requestedRole}**.");
				}

				await executionInfo.message.DeleteAsync();
				return ExecutionStatus.None;
			}
		}
	}
}

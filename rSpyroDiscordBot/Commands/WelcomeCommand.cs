﻿using Discord;
using Discord.WebSocket;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace rSpyroDiscordBot.Commands {
	public class WelcomeCommand : Command {
		public override string baseCommand { get { return "welcome"; } } 
		public override string group { get { return "customization"; } }

		public override CommandFlag settings { get { return CommandFlag.RequireModerator; } }

		public class AddCommand : Command {
			public override string baseCommand { get { return "add"; } }

			public override async Task<ExecutionStatus> OnExecute(ExecutionInfo executionInfo, List<Tuple<int, string>> args) {
				string customMessage = executionInfo.message.Content.Substring(args[0].Item1);

				Settings.GetSettings(executionInfo.guild, out Settings.ServerSettings serverSettings);

				serverSettings.welcomeMessages.Add(customMessage);

				await executionInfo.channel.SendMessageAsync($"Added new custom welcome message:\n\n" + customMessage.Replace("{user}", "__**New User**__"));

				await Settings.Save();

				return ExecutionStatus.None;
			}
		}

		public class RemoveCommand : Command {
			public override string baseCommand { get { return "remove"; } }

			public override async Task<ExecutionStatus> OnExecute(ExecutionInfo executionInfo, List<Tuple<int, string>> args) {
				Settings.GetSettings(executionInfo.guild, out Settings.ServerSettings serverSettings);

				if (int.TryParse(args[0].Item2, out int indexToRemove)) {
					List<string> welcomes = serverSettings.welcomeMessages;

					if (indexToRemove > 0 && indexToRemove <= welcomes.Count) {
						string fullCustomMessage = welcomes[indexToRemove - 1];

						serverSettings.welcomeMessages.RemoveAt(indexToRemove);

						await executionInfo.channel.SendMessageAsync($"Removed custom welcome message:\n\n" + fullCustomMessage.Replace("{user}", "__**New User**__"));
					} else {
						await executionInfo.channel.SendMessageAsync("__**Index out of range**__");
					}
				} else {
					string customMessage = executionInfo.message.Content.Substring(args[0].Item1);
					indexToRemove = serverSettings.welcomeMessages.FindIndex((x) => x.StartsWith(customMessage));

					if (indexToRemove > 0) {
						string fullCustomMessage = serverSettings.welcomeMessages[indexToRemove];

						serverSettings.welcomeMessages.RemoveAt(indexToRemove);

						await executionInfo.channel.SendMessageAsync($"Removed custom welcome message:\n\n" + fullCustomMessage.Replace("{user}", "__**New User**__"));
					} else {
						await executionInfo.channel.SendMessageAsync($"Couldn't remove welcome message that started with:\n\n" + customMessage);
					}
				}

				await Settings.Save();

				return ExecutionStatus.None;
			}
		}

		public class ListCommand : Command {
			public override string baseCommand { get { return "list"; } }

			public override async Task<ExecutionStatus> OnExecute(ExecutionInfo executionInfo, List<Tuple<int, string>> args) {
				if (Settings.GetSettings(executionInfo.guild, out Settings.ServerSettings serverSettings, false) && serverSettings.welcomeMessages.Count > 0) {
					int page = 0;
					if (args.Count > 0)
						int.TryParse(args[0].Item2, out page);

					List<string> welcomes = serverSettings.welcomeMessages;
					int maxPages = (int)MathF.Ceiling((float)welcomes.Count / 10);
					if (page >= 0 && page < maxPages) {
						EmbedBuilder embed = new EmbedBuilder();
						embed.WithTitle($"Welcome List (Page {page + 1} of {maxPages})");

						for (int i = 10 * page; i < Math.Min(welcomes.Count, 10 * (page + 1)); i++) {
							embed.Description += $"**{(i + 1).ToString()}.** " + welcomes[i].Replace("{user}", "__**New User**__") + '\n';
						}

						await executionInfo.channel.SendMessageAsync("", embed: embed.Build());

					} else {
						Program.TemporaryMessage(executionInfo.channel, "Page selected is __out of range__");
					}
				} else {
					Program.TemporaryMessage(executionInfo.channel, $"There are **no welcome messages** set on this guild!");
				}

				await executionInfo.message.DeleteAsync();

				return ExecutionStatus.None;
			}
		}

		public class SetCommand : Command {
			public override string baseCommand { get { return "setchannel"; } }

			public override async Task<ExecutionStatus> OnExecute(ExecutionInfo executionInfo, List<Tuple<int, string>> args) {
				Settings.GetSettings(executionInfo.guild, out Settings.ServerSettings serverSettings);
				Settings.SetChannel(executionInfo, args, ref serverSettings.welcomeChannelId, "display custom welcome messages");

				await Settings.Save();
				await executionInfo.message.DeleteAsync();

				return ExecutionStatus.None;
			}
		}
	}

	
}

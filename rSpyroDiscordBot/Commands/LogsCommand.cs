﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Discord.WebSocket;

namespace rSpyroDiscordBot.Commands {
	public class LogsCommand : Command {
		public override string baseCommand { get { return "log"; } }
		public override string group { get { return "moderation"; } }

		public override CommandFlag settings { get { return CommandFlag.RequireModerator; } }

		public class SetChannelCommand : Command {
			public override string baseCommand { get { return "setchannel"; } }

			public override async Task<ExecutionStatus> OnExecute(ExecutionInfo executionInfo, List<Tuple<int, string>> args) {
				Settings.GetSettings(executionInfo.guild, out Settings.ServerSettings serverSettings);
				Settings.SetChannel(executionInfo, args, ref serverSettings.loggingChannelId, "log server activity");

				await Settings.Save();
				await executionInfo.message.DeleteAsync();

				return ExecutionStatus.ModifiedSettings;
			}
		}

		public class ChannelToggleLoggingCommand : Command {
			public override string baseCommand { get { return "togglechannels"; } }

			public override async Task<ExecutionStatus> OnExecute(ExecutionInfo executionInfo, List<Tuple<int, string>> args) {
				Settings.GetSettings(executionInfo.guild, out Settings.ServerSettings serverSettings);
				Settings.SetChannels(executionInfo, args, serverSettings.watchChannelIdsBlacklist, "watch for log records", true);

				await Settings.Save();
				await executionInfo.message.DeleteAsync();

				return ExecutionStatus.ModifiedSettings;
			}
		}
	}
}

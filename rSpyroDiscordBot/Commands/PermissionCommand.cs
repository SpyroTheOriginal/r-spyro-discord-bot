﻿using Discord.WebSocket;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace rSpyroDiscordBot.Commands {
	public class PermissionCommand : Command {
		public override string baseCommand { get { return "permission"; } }
		public override string group { get { return "moderation"; } }

		public override CommandFlag settings { get { return CommandFlag.RequireAdministrator; } }

		public override async Task<ExecutionStatus> OnExecute(ExecutionInfo executionInfo, List<Tuple<int, string>> args) {
			if (args.Count > 0) {
				SocketRole requestedRole = executionInfo.guild.Roles.FirstOrDefault((x) => x.Name.StartsWith(args[0].Item2, StringComparison.CurrentCultureIgnoreCase));
				Settings.GetSettings(executionInfo.guild, out Settings.ServerSettings serverSettings);

				if (serverSettings.moderatorRoleIds.Contains(requestedRole.Id)) {
					serverSettings.moderatorRoleIds.Remove(requestedRole.Id);
					Program.TemporaryMessage(executionInfo.channel, $"The role __{requestedRole.Name}__ now **cannot** use certain moderation commands.");
				} else {
					serverSettings.moderatorRoleIds.Add(requestedRole.Id);
					Program.TemporaryMessage(executionInfo.channel, $"The role __{requestedRole.Name}__ can now use certain moderation commands.");
				}

				await Settings.Save();

				return ExecutionStatus.ModifiedSettings;
			} else {
				Settings.GetSettings(executionInfo.guild, out Settings.ServerSettings serverSettings, false);

				string msg = $"Roles with moderation permissions:";

				foreach (ulong roleId in serverSettings.moderatorRoleIds) {
					msg += $"\n\t**{executionInfo.guild.GetRole(roleId).Name}**";
				}

				Program.TemporaryMessage(executionInfo.channel, msg);

				return ExecutionStatus.None;
			}
		}
	}
}

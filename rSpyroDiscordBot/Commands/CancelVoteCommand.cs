﻿using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rSpyroDiscordBot.Commands {
	public class CancelVoteCommand : Command {
		public override string baseCommand { get { return "cancelvote"; } }
		public override string group { get { return "moderation"; } }

		public override CommandFlag settings { get { return CommandFlag.RequireModerator; } }

		public override async Task<ExecutionStatus> OnExecute(ExecutionInfo executionInfo, List<Tuple<int, string>> args) {
			if (Program.voteCts != null && !Program.voteCts.IsCancellationRequested) {
				Program.voteCts.Cancel();
				Program.TemporaryMessage(executionInfo.channel, "Vote was canceled by moderation.");
			} else {
				Program.TemporaryMessage(executionInfo.channel, "There is no vote being held right now.");
			}

			await executionInfo.message.DeleteAsync();

			return ExecutionStatus.None;
		}
	}
}

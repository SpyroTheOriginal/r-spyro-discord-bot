﻿using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rSpyroDiscordBot.Commands {
	public class UserVotePinChannelToggleCommand : Command {
		public override string baseCommand { get { return "voteuserpin"; } }
		public override string group { get { return "moderation"; } }

		public override CommandFlag settings { get { return CommandFlag.RequireModerator; } }

		public override async Task<ExecutionStatus> OnExecute(ExecutionInfo executionInfo, List<Tuple<int, string>> args) {
			Settings.GetSettings(executionInfo.guild, out Settings.ServerSettings serverSettings);
			Settings.SetChannels(executionInfo, args, serverSettings.userVoteChannelIdsBlacklist, "prevent user vote pin", true);

			await executionInfo.message.DeleteAsync();

			return ExecutionStatus.ModifiedSettings;
		}
	}

	
}

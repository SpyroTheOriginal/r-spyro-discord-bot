﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using Newtonsoft.Json;

namespace rSpyroDiscordBot.Commands {
	public class VoiceChatOnlyCommand : Command {
		public override string baseCommand { get { return "voice"; } }
		public override string group { get { return "moderation"; } }

		public override CommandFlag settings { get { return CommandFlag.RequireAdministrator; } }

		public override async Task<ExecutionStatus> OnExecute(ExecutionInfo executionInfo, List<Tuple<int, string>> args) {
			Settings.GetSettings(executionInfo.guild, out Settings.ServerSettings serverSettings);

			ulong? oldId = serverSettings.voiceChatOnlyChannelId;
			Settings.SetChannel(executionInfo, args, ref serverSettings.voiceChatOnlyChannelId, "automatically allow muted VC users to chat");

			if (oldId != serverSettings.voiceChatOnlyChannelId) {
				if (serverSettings.voiceChatOnlyChannelId != null) {
					SocketTextChannel newChannel = executionInfo.guild.GetTextChannel(serverSettings.voiceChatOnlyChannelId.Value);

					OverwritePermissions canChatPermissions = new OverwritePermissions(viewChannel: PermValue.Allow, sendMessages: PermValue.Allow);

					foreach (SocketVoiceChannel vcChannel in executionInfo.guild.VoiceChannels) {
						foreach (SocketGuildUser user in vcChannel.Users) {
							if (user.VoiceState.HasValue) {
								SocketVoiceState voiceState = user.VoiceState.Value;
								if (!(voiceState.IsDeafened || voiceState.IsSelfDeafened || voiceState.IsSuppressed)) {
									await newChannel.AddPermissionOverwriteAsync(user, canChatPermissions);
								}
							}
						}
					}
				} else if (oldId != null) {
					SocketTextChannel oldChannel = executionInfo.guild.GetTextChannel(oldId.Value);
					// Users that are already permitted to read and send messages in the VC text channel
					IEnumerable<SocketGuildUser> permittedUserIds = oldChannel.PermissionOverwrites.Where(x => x.TargetType == PermissionTarget.User).Select(x => executionInfo.guild.GetUser(x.TargetId));

					foreach (SocketGuildUser user in permittedUserIds) {
						await oldChannel.RemovePermissionOverwriteAsync(user);
					}
				}
			}

			await Settings.Save();
			await executionInfo.message.DeleteAsync();

			return ExecutionStatus.ModifiedSettings;
		}
	}
}
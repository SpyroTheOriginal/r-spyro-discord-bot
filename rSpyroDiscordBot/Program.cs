﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using rSpyroDiscordBot.Commands;
using Discord.Rest;

namespace rSpyroDiscordBot {
	class Program {
		static void Main(string[] args) {
			string token = "";
			if (args.Length == 0) {
				DiscordLog("Please provide a valid token: ", "Login");
				token = Console.In.ReadLine();
			} else {
				token = args[0];
			}
			new Program().RunBotAsync(token).GetAwaiter().GetResult();
		}

		static bool running = true;

		// Discord Service
		static DiscordSocketClient _client;
		IServiceProvider _services;

		// Settings and Info
		public static Metrics metrics;
		public static readonly string metricsPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "/metrics.json";

		public static Random generator = new Random();

		// TODO: Make values per server instead of global
		public static Task voteTask;
		public static CancellationTokenSource voteCts;

		public static List<Tuple<SocketGuildUser, RestUserMessage, DateTime>> recentUserJoins = new List<Tuple<SocketGuildUser, RestUserMessage, DateTime>>();

		public async Task RunBotAsync(string token) {
			await Settings.Load();

			if (File.Exists(metricsPath)) {
				string fileContent = await File.ReadAllTextAsync(metricsPath);
				metrics = JsonConvert.DeserializeObject<Metrics>(fileContent, new JsonSerializerSettings { MissingMemberHandling = MissingMemberHandling.Ignore });
			} else {
				metrics = new Metrics();
			}

			_client = new DiscordSocketClient(new DiscordSocketConfig() { DefaultRetryMode = RetryMode.AlwaysRetry, MessageCacheSize = 1000 });

			_services = new ServiceCollection()
				.AddSingleton(_client)
				.BuildServiceProvider();

			#region Register Commands
			new PermissionCommand();
			new CancelVoteCommand();
			new UserVotePinChannelToggleCommand();
			new VoiceChatOnlyCommand();
			new MetricsCommand();

			var logs = new LogsCommand();
			logs.RegisterSubCommand(new LogsCommand.SetChannelCommand());
			logs.RegisterSubCommand(new LogsCommand.ChannelToggleLoggingCommand());

			var welcome = new WelcomeCommand();
			welcome.RegisterSubCommand(new WelcomeCommand.SetCommand());
			welcome.RegisterSubCommand(new WelcomeCommand.AddCommand());
			welcome.RegisterSubCommand(new WelcomeCommand.RemoveCommand());
			welcome.RegisterSubCommand(new WelcomeCommand.ListCommand());

			var rolereq = new RoleRequestCommand();
			rolereq.RegisterSubCommand(new RoleRequestCommand.SetChannelCommand());
			rolereq.RegisterSubCommand(new RoleRequestCommand.AddRoleCommand());
			rolereq.RegisterSubCommand(new RoleRequestCommand.RemoveRoleCommand());
			#endregion
			
			// Subscribe to events
			_client.Log += Log;

			_client.UserJoined += OnUserJoinedAsync;
			_client.UserLeft += OnUserLeftAsync;

			_client.MessageReceived += HandleMessageAsync;
			_client.MessageReceived += RecordMessageActivityAsync;

			_client.MessageUpdated += LogMessageUpdateAsync;
			_client.MessageDeleted += LogMessageRemoveAsync;

			_client.ReactionRemoved += HandleReactionRemoveAsync;
			_client.ReactionAdded += HandleReactionAddAsync;

			_client.UserVoiceStateUpdated += OnUserVoiceUpdateAsync;

			_client.LoggedIn += OnConnectionAsync;
			_client.Ready += OnReadyAsync;


			await _client.LoginAsync(TokenType.Bot, token);
			await _client.StartAsync();
			
			UpdateMetrics();
			while (running)
				await OnConsoleInputAsync(await Console.In.ReadLineAsync());

			GetUserMetrics();
			await _client.StopAsync();
			await File.WriteAllTextAsync(metricsPath, JsonConvert.SerializeObject(metrics, Formatting.Indented));
		}

		async Task OnConsoleInputAsync(string input) {
			if (input == "stop") {
				running = false;
			} else if (input == "reload") {
				DiscordLog("Reloading...", "Settings");
				await Settings.Load();
				if (File.Exists(Settings.path)) {
					DiscordLog("Successfully Loaded!", "Settings");
				} else {
					DiscordLog("No settings file found, starting fresh...", "Settings");
				}
			} else {
				DiscordLog(input, "Echo");
			}
		}

		#region Metrics
		public Metrics.ServerHourlyData hourlyData(SocketGuild guild) {
			DateTime now = DateTime.Now.ToUniversalTime();
			DateTime hourNow = now.Date.AddHours(now.Hour);

			if (!Settings.GetSettings(guild, out Settings.ServerSettings serverSettings, false) || serverSettings.metricsEnable == false)
				return null;

			Metrics.ServerHourlyData _hourlyData;
			if (metrics.data.ContainsKey(hourNow)) {
				if (metrics.data[hourNow].ContainsKey(guild.Id)) {
					_hourlyData = metrics.data[hourNow][guild.Id];
				} else {
					_hourlyData = new Metrics.ServerHourlyData();

					metrics.data[hourNow].Add(guild.Id, _hourlyData);
				}
			} else {
				_hourlyData = new Metrics.ServerHourlyData();
				Dictionary<ulong, Metrics.ServerHourlyData> sd = new Dictionary<ulong, Metrics.ServerHourlyData>();
				sd.Add(guild.Id, _hourlyData);
				metrics.data.Add(hourNow, sd);
			}
			return _hourlyData;
		}

		async Task UpdateMetrics() {
			while (running) {
				DateTime now = DateTime.Now.ToUniversalTime();
				DateTime hourNow = now.Date.AddHours(now.Hour); // current hour

				DateTime nextTime = hourNow.AddMinutes(45); // 45 minutes of the hour
				if (now >= nextTime)
					nextTime = nextTime.AddHours(1);
				
				await Task.Delay(nextTime - now);
				DiscordLog("Saving hourly data...", "Metrics");
				GetUserMetrics();
				await File.WriteAllTextAsync(metricsPath, JsonConvert.SerializeObject(metrics, Formatting.Indented));
				DiscordLog("Data Saved!", "Metrics");
			}
		}

		void GetUserMetrics() {
			foreach (var guild in _client.Guilds) {
				// Make sure that the admins agree to have metrics made about their server
				// because information will be constantly stored and storage is limited
				if (!Settings.GetSettings(guild, out Settings.ServerSettings serverSettings, false) || serverSettings.metricsEnable == false)
					continue;

				Metrics.ServerHourlyData sd = hourlyData(guild);
				sd.users = guild.MemberCount;
				foreach (SocketGuildUser user in guild.Users.Where(x => x.VoiceState.HasValue)) {
					if (!sd.activeVCUsers.Contains(user.Id) && user.VoiceState.Value.VoiceChannel.Id != guild.AFKChannel.Id)
						sd.activeVCUsers.Add(user.Id);
				}
				sd.onlineUsers = guild.Users.Where(x => x.Status == UserStatus.Online).Count();
				sd.awayUsers = guild.Users.Where(x => x.Status == UserStatus.Idle || x.Status == UserStatus.AFK).Count();
				sd.busyUsers = guild.Users.Where(x => x.Status == UserStatus.DoNotDisturb).Count();
			}
		}

		Task RecordMessageActivityAsync(SocketMessage sMessage) {
			SocketUserMessage message = sMessage as SocketUserMessage;

			if (message is null || message.Author.IsBot)
				return Task.CompletedTask;

			SocketGuild guild = (message.Channel as SocketGuildChannel)?.Guild as SocketGuild;
			if (guild == null)
				return Task.CompletedTask;

			Metrics.ServerHourlyData hData = hourlyData(guild);
			if (hData == null)
				return Task.CompletedTask;

			hData.postCount++;
			hData.characterCount += message.Content.Length;
			if (!hData.activeUsers.Contains(sMessage.Author.Id))
				hData.activeUsers.Add(sMessage.Author.Id);
			if (!hData.activeChannels.Contains(sMessage.Channel.Id))
				hData.activeChannels.Add(sMessage.Channel.Id);
			return Task.CompletedTask;
		}
		#endregion

		#region Logging/Debug
		Task Log(LogMessage arg) {
			Console.WriteLine(arg);

			return Task.CompletedTask;
		}

		public static void DiscordLog(string value, string prefix = "Info", ConsoleColor textColor = ConsoleColor.Gray) {
			Console.ForegroundColor = textColor;
			Console.WriteLine(DateTime.Now.ToString("HH:mm:ss") + ' ' + String.Format("{0,-12}", prefix) + value);
			Console.ForegroundColor = ConsoleColor.Gray;
		}
		#endregion

		async Task OnReadyAsync() {
			DiscordLog($"Discord Bot '{_client.CurrentUser.Username}' is now operating!");

			foreach (SocketGuild guild in _client.Guilds) {
				if (!Settings.GetSettings(guild, out Settings.ServerSettings serverSettings, false) || serverSettings.voiceChatOnlyChannelId == null)
					continue;

				SocketTextChannel voiceTextChannel = guild.GetTextChannel(serverSettings.voiceChatOnlyChannelId.Value);
				if (voiceTextChannel == null)
					continue;

				List<SocketGuildUser> activeListeningUsers = new List<SocketGuildUser>();
				foreach (SocketVoiceChannel vcChannel in guild.VoiceChannels) {
					foreach (SocketGuildUser user in vcChannel.Users) {
						if (user.VoiceState.HasValue) {
							SocketVoiceState voiceState = user.VoiceState.Value;
							if (!(voiceState.IsDeafened || voiceState.IsSelfDeafened || voiceState.IsSuppressed)) {
								activeListeningUsers.Add(user);
							}
						}
					}
				}

				// Users that are already permitted to read and send messages in the VC text channel
				IEnumerable<SocketGuildUser> permittedUserIds = voiceTextChannel.PermissionOverwrites.Where(x => x.TargetType == PermissionTarget.User).Select(x => guild.GetUser(x.TargetId));

				foreach (SocketGuildUser user in activeListeningUsers.Where(x => !permittedUserIds.Any(y => x.Id == y.Id))) {
					await voiceTextChannel.AddPermissionOverwriteAsync(user, Constants.readWriteHistory);
				}

				// Remove all users that are not actively listening in any voice channel
				foreach (SocketGuildUser user in permittedUserIds.Where(x => !activeListeningUsers.Any(y => x.Id == y.Id))) {
					await voiceTextChannel.RemovePermissionOverwriteAsync(user);
				}
			}
		}

		async Task OnConnectionAsync() {
			await _client.SetGameAsync("over the Dragon Realms", type: ActivityType.Watching);
		}

		async Task OnUserVoiceUpdateAsync(SocketUser user, SocketVoiceState stateBefore, SocketVoiceState stateAfter) {
			SocketGuildUser guildUser = user as SocketGuildUser;
			if (guildUser == null || guildUser.IsBot)
				return;

			if (Settings.GetSettings(guildUser.Guild, out Settings.ServerSettings serverSettings, false) && serverSettings.voiceChatOnlyChannelId != null) {
				SocketTextChannel vcTextChannel = guildUser.Guild.GetTextChannel(serverSettings.voiceChatOnlyChannelId.Value);
				if (vcTextChannel != null) {
					OverwritePermissions? userPermissions = vcTextChannel.GetPermissionOverwrite(user);

					if (stateAfter.VoiceChannel == null || stateAfter.IsDeafened || stateAfter.IsSelfDeafened || stateAfter.IsSuppressed) {
						if (userPermissions != null)
							await vcTextChannel.RemovePermissionOverwriteAsync(user);
					} else if (userPermissions == null) {
						OverwritePermissions canChatPermissions = new OverwritePermissions(viewChannel: PermValue.Allow, sendMessages: PermValue.Allow);
						await vcTextChannel.AddPermissionOverwriteAsync(user, Constants.readWriteHistory);
					}
				}
			}

			if (stateBefore.VoiceChannel == null) { // User joined VC
				DiscordLog($"{user} has joined '{stateAfter.VoiceChannel.Guild.Name}.{stateAfter.VoiceChannel.Name}'", "Voice");

				Metrics.ServerHourlyData sd = hourlyData(guildUser.Guild);
				if (sd != null && !sd.activeVCUsers.Contains(user.Id) && stateAfter.VoiceChannel.Id != guildUser.Guild.AFKChannel.Id)
					sd.activeVCUsers.Add(user.Id);
			} else if (stateAfter.VoiceChannel == null) { // User left VC
				DiscordLog($"{user} has left '{stateBefore.VoiceChannel.Guild.Name}.{stateBefore.VoiceChannel.Name}'", "Voice");

				// If user left, then they technically was active
				Metrics.ServerHourlyData sd = hourlyData(guildUser.Guild);
				if (sd != null && !sd.activeVCUsers.Contains(user.Id) && stateBefore.VoiceChannel.Id != guildUser.Guild.AFKChannel.Id)
					sd.activeVCUsers.Add(user.Id);
			} else if (stateBefore.VoiceChannel.Id != stateAfter.VoiceChannel.Id) { // User changed VC
				DiscordLog($"{user} moved from '{stateBefore.VoiceChannel.Guild.Name}.{stateBefore.VoiceChannel.Name}' to '{stateAfter.VoiceChannel.Guild.Name}.{stateAfter.VoiceChannel.Name}'", "Voice");
			}
		}

		async Task OnUserJoinedAsync(SocketGuildUser user) {
			if (Settings.GetSettings(user.Guild, out Settings.ServerSettings serverSettings, false) && serverSettings.welcomeChannelId != null && serverSettings.welcomeMessages.Count > 0) {
				SocketTextChannel channel = user.Guild.GetChannel(serverSettings.welcomeChannelId.Value) as SocketTextChannel;

				int i = generator.Next(0, serverSettings.welcomeMessages.Count);

				RestUserMessage welcomeMessage = await channel.SendMessageAsync(serverSettings.welcomeMessages[i].Replace("{user}", user.Mention)) as RestUserMessage;

				recentUserJoins.Add(new Tuple<SocketGuildUser, RestUserMessage, DateTime>(user, welcomeMessage, DateTime.Now.AddMinutes(5)));
				if (recentUserJoins.Count > 10)
					recentUserJoins.RemoveAt(0);
			}
		}

		async Task OnUserLeftAsync(SocketGuildUser user) {
			int messageToBeModifiedIndex = recentUserJoins.FindIndex(x => x.Item1.Id == user.Id && x.Item3 > DateTime.Now);

			if (messageToBeModifiedIndex > -1) {
				RestUserMessage welcomeMessage = recentUserJoins[messageToBeModifiedIndex].Item2;
				await welcomeMessage.ModifyAsync(x => x.Content = $"~~{welcomeMessage.Content}~~");
				recentUserJoins.RemoveAt(messageToBeModifiedIndex);
			}
		}

		async Task HandleMessageAsync(SocketMessage sMessage) {
			SocketUserMessage message = sMessage as SocketUserMessage;

			if (message is null || message.Author.IsBot)
				return;

			SocketTextChannel textChannel = message.Channel as SocketTextChannel;
			if (textChannel != null) {
				//int arg = 0;
				//bool isCommand = message.HasStringPrefix("'tests", ref arg) || message.HasMentionPrefix(_client.CurrentUser as IUser, ref arg);

				bool isCommand = message.Content.StartsWith("'s", StringComparison.CurrentCultureIgnoreCase) ||
					message.Content.StartsWith("<@" + _client.CurrentUser.Id + '>', StringComparison.CurrentCultureIgnoreCase); // Apparently has mention prefix doesn't work

				if (isCommand) {
					MatchCollection matches = Regex.Matches(message.Content, @"[\""](.+?)[\""]|([^ ]+)");
					List<Tuple<int, string>> args = new List<Tuple<int, string>>();

					foreach (Match match in matches)
						args.Add(new Tuple<int, string>(match.Captures[0].Index, match.Captures[0].Value));
					args.RemoveAt(0);

				
					await Command.Execute(message.Author, message.Channel as SocketTextChannel, (message.Channel as SocketGuildChannel)?.Guild as SocketGuild, args, message);
				
					return;
				}
				try {
					await HandleRoleRequest(message, textChannel);
				} catch (Exception e) {
					DiscordLog($"{e}", "Error", ConsoleColor.Red);
				}
			}

			SocketDMChannel dmChannel = message.Channel as SocketDMChannel;
			if (dmChannel != null) {
				if (("guilds").StartsWith(message.Content)) {
					IEnumerable<SocketGuild> relaventGuilds = _client.Guilds.Where((x) => { SocketGuildUser guser = x.GetUser(message.Author.Id); return UserIsModerator(guser); });
					await dmChannel.SendMessageAsync(text: $"You have moderation permissions for this bot in {relaventGuilds.Count()} servers:");

					foreach (SocketGuild guild in relaventGuilds) {
						EmbedBuilder embed = new EmbedBuilder();
						embed.WithTitle(guild.Name);
						embed.WithThumbnailUrl(guild.IconUrl);
						embed.WithFooter(guild.Owner.ToString(), guild.Owner.GetAvatarUrl());
						embed.WithTimestamp(guild.CreatedAt);
						string visibleName = guild.GetUser(_client.CurrentUser.Id).Nickname;
						if (string.IsNullOrEmpty(visibleName))
							visibleName = guild.GetUser(_client.CurrentUser.Id).Username;
						embed.WithDescription("**Bot's Current Name: **" + visibleName);

						embed.AddField("Members", guild.MemberCount, true);

						await dmChannel.SendMessageAsync(embed: embed.Build());
					}
				} else if (("invite").StartsWith(message.Content)) {
					await dmChannel.SendMessageAsync("You can add me to a server where you have administrative rights using this OAuth2 link:\n" +
						$"https://discordapp.com/api/oauth2/authorize?client_id={_client.CurrentUser.Id}&scope=bot");
					
				}
			}
		}

		async Task LogMessageUpdateAsync(Cacheable<IMessage, ulong> messageBeforeCache, SocketMessage messageAfter, ISocketMessageChannel channel) {
			SocketTextChannel guildChannel = channel as SocketTextChannel;
			if (guildChannel == null || messageAfter.Author == null || messageAfter.Author.IsBot)
				return;

			if (Settings.GetSettings(guildChannel.Guild, out Settings.ServerSettings serverSettings, false) && serverSettings.loggingChannelId != null) {
				SocketTextChannel loggingChannel = guildChannel.Guild.GetTextChannel(serverSettings.loggingChannelId.Value);
				if (loggingChannel == null)
					return;

				bool contentModified = true;
				Attachment[] attachementsAdded = messageAfter.Attachments.ToArray();

				EmbedBuilder embed = new EmbedBuilder();

				embed.WithAuthor(messageAfter.Author);
				embed.WithDescription($"Edited message in {guildChannel.Mention}");
				embed.WithColor(new Color(1f, 0.5f, 0f));

				if (messageBeforeCache.HasValue) {
					SocketMessage messageBefore = messageBeforeCache.Value as SocketMessage;
					contentModified = messageAfter.Content != messageBefore.Content;

					// DiscordLog(messageAfter.Content);
					// DiscordLog(messageBefore.Content);

					if (contentModified)
						embed.AddField("Before", messageBeforeCache.Value.Content);

					// Remove attachments that already existed in the new message
					Attachment[] attachementsRemoved = messageBefore.Attachments.Where(x => !messageAfter.Attachments.Contains(x)).ToArray();
					if (attachementsRemoved.Length > 0) {
						string attachments = "";
						foreach (Attachment attachment in attachementsRemoved) {
							attachments += $"{attachment.Filename}: {attachment.Url}\n";
						}
						embed.AddField("Attachments Removed", attachments);

					}
					// Remove attachments that already existed in the old message
					attachementsAdded = attachementsAdded.Where(x => !messageBefore.Attachments.Contains(x)).ToArray();
				} else {
					embed.AddField("Before", "`Original message not cached...`");
				}

				if (contentModified)
					embed.AddField("After", messageAfter.Content);

				if (attachementsAdded.Length > 0) {
					string attachments = "";
					foreach (Attachment attachment in attachementsAdded) {
						attachments += $"{attachment.Filename}: {attachment.Url}\n";
					}
					embed.AddField("Attachments Added", messageAfter.Content);
				}

				embed.WithFooter($"ID: {messageBeforeCache.Id}");

				await loggingChannel.SendMessageAsync("", embed: embed.Build());
			}

			return;
		}

		async Task LogMessageRemoveAsync(Cacheable<IMessage, ulong> deletedMessageCached, ISocketMessageChannel channel) {
			SocketTextChannel guildChannel = channel as SocketTextChannel;
			if (guildChannel == null)
				return;

			if (Settings.GetSettings(guildChannel.Guild, out Settings.ServerSettings serverSettings, false) && serverSettings.loggingChannelId != null) {
				SocketTextChannel loggingChannel = guildChannel.Guild.GetTextChannel(serverSettings.loggingChannelId.Value);
				if (loggingChannel == null)
					return;

				EmbedBuilder embed = new EmbedBuilder();

				embed.WithDescription($"Removed message in {guildChannel.Mention}");
				if (deletedMessageCached.HasValue) {
					SocketMessage messageBefore = deletedMessageCached.Value as SocketMessage;
					if (messageBefore.Author == null || messageBefore.Author.IsBot)
						return;

					embed.WithAuthor(messageBefore.Author);
					embed.AddField("Message", messageBefore.Content);
					embed.WithColor(new Color(1f, 0f, 0f));

					if (messageBefore.Attachments.Count > 0) {
						string attachments = "";
						foreach (Attachment attachment in messageBefore.Attachments) {
							attachments += $"{attachment.Filename}: {attachment.Url}\n";
						}
						embed.AddField("Attachments", attachments);
					}
					embed.WithTimestamp(messageBefore.CreatedAt);
				} else {
					embed.AddField("Message", "`Original message not cached...`");
				}

				embed.WithFooter($"ID: {deletedMessageCached.Id}");

				await loggingChannel.SendMessageAsync("", embed: embed.Build());
			}
		}

		async Task HandleReactionRemoveAsync(Cacheable<IUserMessage, ulong> arg1, ISocketMessageChannel arg2, SocketReaction reaction) {
			IUserMessage message = await reaction.Channel.GetMessageAsync(arg1.Id) as IUserMessage;
			if (message is null || message.Author.IsBot)
				return;

			if (reaction.Emote.Name == Constants.pushPinEmoji.Name) {
				if (message.IsPinned && message.Reactions[reaction.Emote].ReactionCount == 2)
					await message.UnpinAsync();
			}
		}

		async Task HandleReactionAddAsync(Cacheable<IUserMessage, ulong> arg1, ISocketMessageChannel arg2, SocketReaction reaction) {
			SocketUserMessage message = await reaction.Channel.GetMessageAsync(arg1.Id) as SocketUserMessage;

			if (message is null || message.Author.IsBot)
				return;

			SocketTextChannel channel = message.Channel as SocketTextChannel;
			SocketGuild guild = channel.Guild;

			if (reaction.Emote.Name == Constants.xEmoji.Name && message.IsPinned && (reaction.UserId == message.Author.Id || UserIsModerator(guild.GetUser(reaction.UserId))))
				await message.UnpinAsync();

			if ((!Settings.GetSettings(channel.Guild, out Settings.ServerSettings serverSettings, false) || !serverSettings.userVoteChannelIdsBlacklist.Contains(channel.Id)) && reaction.Emote.Name == Constants.pushPinEmoji.Name) {
				try {
					await HandlePinRequest(reaction, message);
				} catch (Exception e) {
					DiscordLog(e.ToString(), "Error", ConsoleColor.Red);
				}
			}
		}

		async Task HandlePinRequest(SocketReaction sReaction, SocketUserMessage message) {
			ReactionMetadata reaction = message.Reactions[sReaction.Emote];

			if (!message.IsPinned && reaction.ReactionCount == 3) { // Is the limit reached?
				if (voteTask == null || voteTask.IsCompleted) { // Does a vote exists and is it still polling?
																// Get if special users marked the message not pinnable by vote
					IEnumerable<IUser> usersRejected = await message.GetReactionUsersAsync(Constants.xEmoji, 100).FlattenAsync();

					if (usersRejected.Any(x => x.IsBot || x.Id == message.Author.Id || UserIsModerator(x as SocketGuildUser)))
						return;

					// Collect currently pin reaction users
					IUser[] usersReacted = (await message.GetReactionUsersAsync(sReaction.Emote, 100).FlattenAsync()).ToArray();

					string userList = "";
					for (int i = 0; i < usersReacted.Length; i++) {
						IUser user = usersReacted[i];
						if (i == 0) {
							userList += user.Mention;
						} else if (i == usersReacted.Length - 1) {
							userList += $" and {user.Mention}";
						} else if (i > 0) {
							userList += $", {user.Mention}";
						}
					}

					EmbedBuilder embed = new EmbedBuilder();
					embed.WithTitle("Vote to pin this message!");
					embed.WithAuthor(message.Author);
					embed.WithDescription(message.Content.Length > 128 ? message.Content.Substring(0, 128) + "..." : message.Content);
					if (message.Attachments.Count > 0) {
						string firstAttachements = message.Attachments.ElementAt(0).Url;
						if (firstAttachements.EndsWith(".gif") || firstAttachements.EndsWith(".png") || firstAttachements.EndsWith(".jpeg") || firstAttachements.EndsWith(".jpg") || firstAttachements.EndsWith(".bmp")) {
							embed.WithThumbnailUrl(firstAttachements);
						}
					}
					embed.WithFooter($"Message ID: {message.Id.ToString()}");
					embed.WithTimestamp(message.Timestamp);

					string userListConsole = "";
					for (int i = 0; i < usersReacted.Length; i++) {
						IUser user = usersReacted[i];
						if (i == 0) {
							userListConsole += user;
						} else if (i == usersReacted.Length - 1) {
							userListConsole += $" and {user}";
						} else if (i > 0) {
							userListConsole += $", {user}";
						}
					}
					DiscordLog($"{userList} started the vote to pin a message by {message.Author}");

					voteCts = new CancellationTokenSource(); // Make the vote cancelable

					// Add functionality to cancel the vote event
					Func<Cacheable<IUserMessage, ulong>, ISocketMessageChannel, SocketReaction, Task> cancelVoteEvent = async (arg1, arg2, creaction) => {
						if ((creaction.Emote.Name == Constants.thumbsDownEmoji.Name || creaction.Emote.Name == Constants.xEmoji.Name) && creaction.UserId == message.Author.Id) {
							voteCts.Cancel();
							TemporaryMessage((ITextChannel)creaction.Channel, $"Vote canceled by message owner: {message.Author.Mention}\nIf you would like the message not be pinned in the future, react with a {Constants.xEmoji} on the original message");
						}

						if (creaction.Emote.Name == Constants.xEmoji.Name && creaction.MessageId == message.Id && (creaction.User.Value as IGuildUser).GuildPermissions.Administrator) {
							voteCts.Cancel();
							TemporaryMessage((ITextChannel)creaction.Channel, $"Vote canceled by moderation");
						}
					};

					_client.ReactionAdded += cancelVoteEvent; // Subscribe to the reaction event

					//messageToBePinned = message;
					voteTask = VotedAction(
						async (x, y) => {
							await message.PinAsync();
						},
						(SocketTextChannel)message.Channel, $"{userList} wants this message by {message.Author.Mention} to be pinned.\n__Author of the message has power to cancel via downvote__", embed.Build(), settings: VoteSettings.ShowTimer,
							endAction: () => {
								_client.ReactionAdded -= cancelVoteEvent; // Remove self when vote ends
								return Task.CompletedTask;
							}
					);
				} else {

				}
			}
		}

		public static bool UserIsModerator(SocketGuildUser user) {
			return user.Guild.Owner.Id == user.Id || user.GuildPermissions.Administrator || (Settings.GetSettings(user.Guild, out Settings.ServerSettings serverSettings, false) && serverSettings.moderatorRoleIds.Contains(user.Id));
		}

		async Task HandleRoleRequest(SocketUserMessage message, SocketTextChannel channel) {
			SocketGuild guild = channel.Guild;
			if (!(Settings.GetSettings(guild, out Settings.ServerSettings serverSettings, false) && serverSettings.roleRequestChannelId == channel.Id))
				return;

			string msgLower = message.Content.ToLower();

			// Get guild information
			IReadOnlyCollection<SocketRole> allRoles = guild.Roles;
			IEnumerable<SocketRole> availableRoles = allRoles.Where(x => serverSettings.requestableRoleIds.Contains(x.Id));

			// Get user information
			SocketGuildUser guser = message.Author as SocketGuildUser;
			IEnumerable<SocketRole> guserRoles = allRoles.Intersect(guser.Roles);
			if (msgLower == "clear") {
				List<SocketRole> rolesToRemove = new List<SocketRole>(availableRoles.Intersect(guserRoles));
				await guser.RemoveRolesAsync(rolesToRemove);

				TemporaryMessage(channel, $"{message.Author.Mention}, all your requestable roles has been removed!");
			} else {
				SocketRole requestedRole = availableRoles.FirstOrDefault(x => msgLower.StartsWith(x.Name.ToLower()));
				if (requestedRole != null) { // is there a role match
					// Get heiarchy path, if any, to the role requested
					Stack<ulong> requestedRolePath = new Stack<ulong>();
					ulong currentPathId = requestedRole.Id;
					requestedRolePath.Push(currentPathId);
					while (serverSettings.parentedRoleIds.TryGetValue(currentPathId, out currentPathId))
						requestedRolePath.Push(currentPathId);

					// Starting from root, remove any that already exists
					while (guserRoles.Any(x => x.Id == requestedRolePath.Peek()))
						requestedRolePath.Pop();

					// Get roles from the user that could potentially be removed
					List<SocketRole> modifyableRoles = new List<SocketRole>(guserRoles.Intersect(availableRoles));

					// Find the role that is parented to the last in the request path
					SocketRole relatedRole = modifyableRoles.Find(x => serverSettings.parentedRoleIds.TryGetValue(x.Id, out ulong parentId) && parentId == serverSettings.parentedRoleIds[requestedRolePath.Peek()]);

					List<SocketRole> rolesToRemove = new List<SocketRole>();
					if (relatedRole != null) {
						rolesToRemove.Add(relatedRole);

						ulong childRole = serverSettings.parentedRoleIds.FirstOrDefault(x => x.Value == relatedRole.Id).Key;
						while (serverSettings.parentedRoleIds.Any(x => x.Value == relatedRole.Id)) {

							childRole = serverSettings.parentedRoleIds.FirstOrDefault(x => x.Value == childRole).Key;
						}
					}
					//////////////////////////////////////////////////////////////////////////
					try { // Attempt to apply cosmetic roles
						await guser.RemoveRolesAsync(rolesToRemove);

						await guser.AddRoleAsync(requestedRole);

						TemporaryMessage(channel, $"{message.Author.Mention}, the role **{requestedRole.Name}** has been given to you");
						DiscordLog($"{message.Author.Username} took the {requestedRole.Name} role");
					} catch (Exception) { // if failed to apply cosmetic roles
						TemporaryMessage(channel, $"{message.Author.Mention}, sorry! I could not give the role **{requestedRole.Name}**");
					}
				} else if (msgLower.StartsWith("role")) { // is the request asking for role list
					EmbedBuilder embed = new EmbedBuilder { Title = "Available Roles" };
					foreach (IRole role in availableRoles) {
						// Get bot information
						SocketGuildUser selfguser = guild.GetUser(_client.CurrentUser.Id);
						
						SocketRole gatekeepingRole = selfguser.Roles.OrderBy(x => x.Position).Last(); // Used to determine if the role is higher than the bot

						if (role.Position != 0 && role.Position < gatekeepingRole.Position)
							embed.Description += $"{role.Name}\n";
						else
							embed.Description += $"~~{role.Name}~~\n"; // Unassignable due to role position
					}

					Task t = TemporaryMessage(channel, "", embed.Build());
				} else { // is the request random
					Task t = TemporaryMessage(channel,
						"Type `role` or `roles` for a list of available cosmetics roles\n" +
						"or the name of the role to be assigned as that one. `clear` to remove all cosmetics roles.\n" +
						$"**If there are any questions about roles, check pins or ask in <#340174905985204224>**"
					);
				}
			}

			await message.DeleteAsync();
		}

		public static async Task TemporaryMessage(ITextChannel channel, string message, Embed embed = null, int seconds = 10, bool showTimer = false) {
			RestUserMessage messageCreated = await channel.SendMessageAsync(message, embed: embed) as RestUserMessage;
			if (showTimer) {
				for (int i = seconds - 1; i >= 0; i--) {
					await messageCreated.ModifyAsync((MessageProperties x) => {
						x.Content = message + $"\n[This message will show for {i} second(s)]";
					});
					await Task.Delay(1000);
				}
			} else {
				await Task.Delay(1000 * seconds);
			}
			await messageCreated.DeleteAsync();
		}

		[Flags]
		public enum VoteSettings {
			None = 0,
			ShowTimer = 1 << 1,
			AllowMultiple = 1 << 2,
			NoExtraReactions = 1 << 3
		}

		public static async Task VotedAction(Func<IUser[], IUser[], Task> action, SocketTextChannel channel, string message, Embed embed = null, int seconds = 60, VoteSettings settings = VoteSettings.None, Func<Task> endAction = null) {
			DiscordLog($"A vote has begun");
			RestUserMessage messageCreated = await channel.SendMessageAsync(message, embed: embed);
			
			await messageCreated.AddReactionAsync(Constants.thumbsUpEmoji);
			await messageCreated.AddReactionAsync(Constants.thumbsDownEmoji);

			if (settings.HasFlag(VoteSettings.NoExtraReactions)) {
				Func<Cacheable<IUserMessage, ulong>, ISocketMessageChannel, SocketReaction, Task> cancelVoteEvent = async (arg1, arg2, reaction) => {
					if (reaction.Emote.Name != Constants.thumbsUpEmoji.Name && reaction.Emote.Name != Constants.thumbsDownEmoji.Name) {
						await messageCreated.RemoveReactionAsync(reaction.Emote, reaction.User.Value);
					}
				};
				_client.ReactionAdded += cancelVoteEvent; // Subscribe to the reaction event
			}

			// Encapsulate the timer to prevent the cancelation exception from propagating to this task
			try {
				if (settings.HasFlag(VoteSettings.ShowTimer)) {
					for (int i = seconds / 10; i > 1; i--) {
						await messageCreated.ModifyAsync((MessageProperties x) => {
							x.Content = message + $"\n[This message will show for {i * 10} second(s)]";
						});
						await Task.Delay(10000, voteCts.Token);
					}

					for (int i = seconds - ((seconds / 10 - 1) * 10); i >= 0; i-=2) {
						await messageCreated.ModifyAsync((MessageProperties x) => {
							x.Content = message + $"\n[This message will show for {i} second(s)]";
						});
						await Task.Delay(2000, voteCts.Token);
					}
				} else {
					await Task.Delay(1000 * seconds, voteCts.Token);
				}
			} catch (Exception) { }

			if (endAction != null)
				await endAction();

			if (voteCts.IsCancellationRequested) {
				DiscordLog($"Vote Canceled");
			} else {
				IUser[] usersVotedYes = (await messageCreated.GetReactionUsersAsync(Constants.thumbsUpEmoji, 100).FlattenAsync()).ToArray();
				IUser[] usersVotedNo = (await messageCreated.GetReactionUsersAsync(Constants.thumbsDownEmoji, 100).FlattenAsync()).ToArray();

				EmbedBuilder finalEmbed = new EmbedBuilder();
				if (usersVotedYes.Length + usersVotedNo.Length >= 5) {
					if (usersVotedYes.Length > usersVotedNo.Length) {
						await action(usersVotedYes.ToArray(), usersVotedNo.ToArray());

						finalEmbed.Title = $"Vote Successful";
						finalEmbed.Color = Color.Green;
						finalEmbed.WithCurrentTimestamp();

						DiscordLog($"Vote Successful");
					} else {
						finalEmbed.Title = "Vote Failed";
						finalEmbed.Color = Color.Red;
						finalEmbed.WithCurrentTimestamp();

						DiscordLog($"Vote Failed");
					}
				} else {
					finalEmbed.Title = "Not Enough Votes";
					finalEmbed.Color = Color.Gold;
					finalEmbed.WithCurrentTimestamp();
				}
				await TemporaryMessage(channel, "", finalEmbed.Build());
			}

			await messageCreated.DeleteAsync();
		}
	}
}

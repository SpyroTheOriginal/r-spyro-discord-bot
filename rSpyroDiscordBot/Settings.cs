﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Discord.WebSocket;
using Newtonsoft.Json;

namespace rSpyroDiscordBot {
	class Settings {
		public static Settings settings;
		public static readonly string path = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "/settings.json";

		public class ServerSettings {
			/// <summary>
			/// Set whether or not to respond to normal users if the bot has been called.
			/// </summary>
			[JsonProperty("respond")] public bool alwaysRespond = false;

			#region Channels
			/// <summary>
			/// The id of the channel where the welcome messages will appear.
			/// </summary>
			[JsonProperty("welcome")] public ulong? welcomeChannelId;

			/// <summary>
			/// The id of the channel where normal users can request a role to be applied to them.
			/// </summary>
			[JsonProperty("role")] public ulong? roleRequestChannelId;

			/// <summary>
			/// The ids of the channels where user based pins by vote is not allowed or will not occur.
			/// </summary>
			[JsonProperty("no_pin_vote")] public readonly List<ulong> userVoteChannelIdsBlacklist = new List<ulong>();

			/// <summary>
			/// The id of the channel where users that are muted but listening can chat.
			/// </summary>
			[JsonProperty("vc_only")] public ulong? voiceChatOnlyChannelId;

			/// <summary>
			/// The id of the channel where logging of the server activity will be sent to.
			/// </summary>
			[JsonProperty("log")] public ulong? loggingChannelId;

			/// <summary>
			/// The ids of the channels that will not be watched for server logging.
			/// </summary>
			[JsonProperty("no_watch")] public readonly List<ulong> watchChannelIdsBlacklist = new List<ulong>();
			#endregion

			#region Roles
			[JsonProperty("modRoles")] public readonly List<ulong> moderatorRoleIds = new List<ulong>();
			[JsonProperty("askableRoles")] public readonly List<ulong> requestableRoleIds = new List<ulong>();
			[JsonProperty("parentRoles")] public readonly Dictionary<ulong, ulong> parentedRoleIds = new Dictionary<ulong, ulong>();
			#endregion

			[JsonProperty("welcomeMsgs")]
			public readonly List<string> welcomeMessages = new List<string>();

			/*public class ChannelReminder {
				public int limit;
				public int current;
				public ulong? lastReminder;
				public string message;
			}

			[JsonProperty("reminders")]
			public readonly Dictionary<ulong, ChannelReminder> reminderMessages = new Dictionary<ulong, ChannelReminder>();*/

			[JsonProperty("metrics")] public bool metricsEnable = false;
		}
		public readonly Dictionary<ulong, ServerSettings> servers = new Dictionary<ulong, ServerSettings>();

		public static bool GetSettings(SocketGuild guild, out ServerSettings serverSettings, bool createIfNotExist = true) {
			bool exists = settings.servers.TryGetValue(guild.Id, out serverSettings);
			if (!exists && createIfNotExist)
				settings.servers.Add(guild.Id, serverSettings = new ServerSettings());
			return exists;
		}

		public static async Task Save() {
			await File.WriteAllTextAsync(path, JsonConvert.SerializeObject(settings, Formatting.Indented));
		}

		public static async Task<bool> Load() {
			bool exists = File.Exists(path);
			if (exists) {
				string fileContent = await File.ReadAllTextAsync(path);
				settings = JsonConvert.DeserializeObject<Settings>(fileContent, new JsonSerializerSettings { MissingMemberHandling = MissingMemberHandling.Ignore });
			} else {
				settings = new Settings();
			}
			return exists;
		}

		public static void SetChannel(Command.ExecutionInfo executionInfo, List<Tuple<int,string>> args, ref ulong? channelId, string action) {
			if (args.Count > 0 && Constants.clearKeywords.Any(x => x.StartsWith(args[0].Item2, StringComparison.CurrentCultureIgnoreCase))) {
				// Clear the channel Id
				channelId = null;
				Program.TemporaryMessage(executionInfo.channel, $"This server will not {action} anymore.");
			} else {
				// Set the channel Id
				IReadOnlyCollection<SocketGuildChannel> mentionedChannels = executionInfo.message.MentionedChannels;
				channelId = mentionedChannels.Count > 0 ? mentionedChannels.ElementAt(0).Id : executionInfo.channel.Id;
				Program.TemporaryMessage(executionInfo.channel, $"<#{channelId}> will {action}.");
			}
		}

		public static void SetChannels(Command.ExecutionInfo executionInfo, List<Tuple<int, string>> args, List<ulong> channelIds, string action, bool blacklist = false) {
			List<ulong> channelsToModify = new List<ulong>();

			IReadOnlyCollection<SocketGuildChannel> mentionedChannels = executionInfo.message.MentionedChannels;

			if (mentionedChannels.Count > 0) {
				channelsToModify.AddRange(mentionedChannels.Select(x => x.Id));
				args = args.Skip(mentionedChannels.Count).ToList();
			} else {
				channelsToModify.Add(executionInfo.channel.Id);
			}

			bool? toggleState = null;
			if (args.Count > 0 && Constants.toggleOnKeywords.Any(x => x.StartsWith(args[0].Item2, StringComparison.CurrentCultureIgnoreCase)))
				toggleState = true;
			else if (args.Count > 0 && Constants.toggleOffKeywords.Any(x => x.StartsWith(args[0].Item2, StringComparison.CurrentCultureIgnoreCase)))
				toggleState = false;

			if (toggleState.HasValue) {
				string channelList = "";
				for (int i = 0; i < channelsToModify.Count; i++) {
					ulong channelId = channelsToModify[i];
					if (i == 0) {
						channelList += $"<#{channelId}>";
					} else if (i == channelsToModify.Count - 1) {
						channelList += $" and <#{channelId}>";
					} else if (i > 0) {
						channelList += $", <#{channelId}>";
					}
				}

				Program.TemporaryMessage(executionInfo.channel, $"{channelList} will {(toggleState.Value ? "" : "not ")}{action}.");

				if (blacklist)
					toggleState = !toggleState;

				if (toggleState.Value)
					channelIds.AddRange(channelsToModify);
				else
					channelIds.RemoveAll(x => channelsToModify.Contains(x));
			} else {
				string msg = $"Channels where they {action}:";

				if (channelIds.Count > 0) {
					foreach (long channelId in channelIds) {
						msg += $"\n\t<#{channelId}>";
					}
				} else {
					msg += "*none*";
				}

				msg += $"\n\nTo the toggle channels, use: `<channel names, none for current channel>` (`{string.Join("`, `", Constants.toggleOnKeywords)}`) or (`{string.Join("`, `", Constants.toggleOffKeywords)}`)";

				Program.TemporaryMessage(executionInfo.channel, msg);
			}
		}
	}
}

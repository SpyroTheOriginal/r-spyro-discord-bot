﻿using Discord;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rSpyroDiscordBot {
	public abstract class Command {
		public static List<Command> registeredCommands = new List<Command>();

		public abstract string baseCommand { get; }
		public virtual string group { get; }
		[Flags]
		public enum CommandFlag {
			None = 0,
			RequireAdministrator = 1 << 0,
			RequireModerator = 1 << 1,
		}
		public virtual CommandFlag settings { get { return CommandFlag.None; } }

		[Flags]
		public enum ExecutionStatus {
			None = 0,
			ModifiedSettings = 1 << 0,
			SendHelpMessage = 1 << 1,
		}

		public struct ExecutionInfo {
			public readonly SocketUserMessage message;
			public readonly SocketUser user;
			public readonly SocketTextChannel channel;
			public readonly SocketGuild guild;

			public ExecutionInfo(SocketUserMessage message, SocketUser user, SocketTextChannel channel, SocketGuild guild) {
				this.message = message;
				this.user = user;
				this.channel = channel;
				this.guild = guild;
			}
		}

		List<Command> subcommands;

		public Command() {
			if (registeredCommands.Contains(this)) {
				Program.DiscordLog(GetType().Name + " is already a registered");
				return;
			} else if (registeredCommands.Exists(x => x.baseCommand == baseCommand)) {
				Program.DiscordLog(baseCommand + " is already a registered command");
				return;
			}

			registeredCommands.Add(this);
		}

		public static async Task Execute(SocketUser user, SocketTextChannel channel, SocketGuild guild, List<Tuple<int, string>> args, SocketUserMessage originalMessage) {
			if (args.Count > 0) {
				Command command = registeredCommands.Find((x) => x.baseCommand.StartsWith(args[0].Item2, StringComparison.CurrentCultureIgnoreCase));

				if (command == null) {
					await channel.SendMessageAsync($"There is no such command that starts with __{args[0].Item2}__");
				} else {
					string fullCommand = originalMessage.Content.Substring(args[0].Item1);
					Program.DiscordLog($"{originalMessage.Author} executed: {fullCommand}", "Command", ConsoleColor.Cyan);

					if (Settings.GetSettings(guild, out Settings.ServerSettings serverSettings, false) && serverSettings.loggingChannelId != null) {
						SocketTextChannel loggingChannel = guild.GetTextChannel(serverSettings.loggingChannelId.Value);
						if (loggingChannel == null)
							return;

						EmbedBuilder embed = new EmbedBuilder();

						embed.WithAuthor(user);
						embed.WithTitle("Executed Command");
						embed.WithDescription(fullCommand);

						embed.WithColor(new Color(0f, 0.5f, 1f));

						await loggingChannel.SendMessageAsync("", embed: embed.Build());
					}


					SocketGuildUser guser = user as SocketGuildUser;
					if (guser != null && (!command.settings.HasFlag(CommandFlag.RequireModerator) || Program.UserIsModerator(guser)) && (!command.settings.HasFlag(CommandFlag.RequireAdministrator) || guser.GuildPermissions.Administrator)) {
						var subargs = args.Skip(1).ToList();
						try {
							ExecutionStatus status = await command.OnExecute(new ExecutionInfo(originalMessage, user, channel, guild), subargs);
							if (status.HasFlag(ExecutionStatus.SendHelpMessage) && (Program.UserIsModerator(guser) || (!(command.settings.HasFlag(CommandFlag.RequireAdministrator) || command.settings.HasFlag(CommandFlag.RequireModerator)) || (serverSettings != null && serverSettings.alwaysRespond)))) {
								await channel.SendMessageAsync(await command.GenerateUsageMessage(user, channel, guild, subargs, originalMessage));
							}
						} catch (Exception e) {
							Program.DiscordLog($"Failed to execute command: {fullCommand}\n\tFull Message:\n{e.ToString()}", "Error", ConsoleColor.Red);

							Program.TemporaryMessage(channel, $"An internal error has occured with command (**{fullCommand}**): __{e.Message}__");
							if (serverSettings != null && serverSettings.loggingChannelId != null) {
								SocketTextChannel loggingChannel = guild.GetTextChannel(serverSettings.loggingChannelId.Value);
								if (loggingChannel == null)
									return;

								EmbedBuilder embed = new EmbedBuilder();

								embed.WithAuthor(user);
								embed.WithTitle("Error while executing a command");
								embed.WithUrl("https://bitbucket.org/SpyroTheOriginal/r-spyro-discord-bot/issues");
								embed.WithDescription(fullCommand);

								embed.WithColor(new Color(1f, 0f, 0f));

								await loggingChannel.SendMessageAsync("", embed: embed.Build());
								await loggingChannel.SendMessageAsync($"```{e.ToString()}```");
							}
						}
					} else if (Program.UserIsModerator(guser) || (serverSettings != null && serverSettings.alwaysRespond)) {
						Program.TemporaryMessage(channel, $"Sorry! You do not have permission to use this command.");
					}
				}
			} else {
				string message = $"Available Commands:";
				foreach (Command command in registeredCommands)
					message += $"\n\t`{command.baseCommand}`";

				await channel.SendMessageAsync(message);
			}
		}

		public void RegisterSubCommand(Command subcommand) {
			if (subcommands != null) {
				if (subcommands.Contains(this)) {
					Program.DiscordLog(GetType().Name + " is already a registered");
					return;
				} else if (subcommands.Exists(x => x.baseCommand == baseCommand)) {
					Program.DiscordLog(baseCommand + " is already a registered subcommand");
					return;
				}
			}

			if (subcommands == null)
				subcommands = new List<Command>();
			subcommands.Add(subcommand);

			subcommands.Sort((x, y) => x.baseCommand.CompareTo(y.baseCommand));

			registeredCommands.Remove(subcommand);
		}

		public virtual async Task<ExecutionStatus> OnExecute(ExecutionInfo executionInfo, List<Tuple<int, string>> args) {
			if (subcommands == null) {
				executionInfo.channel.SendMessageAsync("This command currently has no function implemented!");
				return ExecutionStatus.None;
			} else if (args.Count > 0) {
				Command subcommand = subcommands.Find((x) => x.baseCommand.StartsWith(args[0].Item2, StringComparison.CurrentCultureIgnoreCase));

				if (subcommand != null) {
					SocketGuildUser guser = executionInfo.user as SocketGuildUser;
					if (guser != null && (!subcommand.settings.HasFlag(CommandFlag.RequireModerator) || Program.UserIsModerator(guser)) && (!subcommand.settings.HasFlag(CommandFlag.RequireAdministrator) || guser.GuildPermissions.Administrator))
						return await subcommand.OnExecute(executionInfo, args.Skip(1).ToList());
					else
						Program.TemporaryMessage(executionInfo.channel, $"Sorry! You do not have permission to use this command.");
				}
			}

			return ExecutionStatus.SendHelpMessage;
		}

		public virtual async Task<string> GenerateUsageMessage(SocketUser user, SocketTextChannel channel, SocketGuild guild, List<Tuple<int, string>> args, SocketUserMessage originalMessage) {
			if (args.Count > 0) {
				string reduced = args[0].Item2;
				while (reduced.Length > 0) {
					if (subcommands.Exists((x) => x.baseCommand.StartsWith(reduced, StringComparison.CurrentCultureIgnoreCase)))
						break;
					reduced = reduced.Substring(0, reduced.Length - 1);
				}

				if (reduced.Length > 0) {
					string message = "Did you mean:";
					foreach (Command subcommand in subcommands) {
						if (subcommand.baseCommand.StartsWith(args[0].Item2, StringComparison.CurrentCultureIgnoreCase))
							message += $"\n\t`{subcommand.baseCommand}`";
					}
					return message;
				} else {
					return "Sorry, that command does not exist.";
				}
			} else {
				string message = $"`{baseCommand}` subcommands:";
				foreach (Command subcommand in subcommands)
					message += $"\n\t`{subcommand.baseCommand}`";

				return message;
			}
		}
	}
}

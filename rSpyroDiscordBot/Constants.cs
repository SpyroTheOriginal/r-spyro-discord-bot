﻿using System;
using System.Collections.Generic;
using System.Text;
using Discord;

namespace rSpyroDiscordBot {
	public struct Constants {
		public static readonly string[] toggleOnKeywords = new string[] { "on", "yes", "true", "enable" };
		public static readonly string[] toggleOffKeywords = new string[] { "off", "no", "false", "disable" };
		public static readonly string[] clearKeywords = { "clear", "none" };

		public static readonly Emoji pushPinEmoji = new Emoji("📌");
		public static readonly Emoji thumbsUpEmoji = new Emoji("👍");
		public static readonly Emoji thumbsDownEmoji = new Emoji("👎");
		public static readonly Emoji xEmoji = new Emoji("❌");

		public static readonly OverwritePermissions readWriteHistory = new OverwritePermissions(viewChannel: PermValue.Allow, sendMessages: PermValue.Allow, readMessageHistory: PermValue.Allow);

	}
}

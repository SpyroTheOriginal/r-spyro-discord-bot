# rSpyro-Bot
[![Discord](https://img.shields.io/discord/281047897380880384.svg?label=r/Spyro&logo=discord&colorB=7289DA)](https://discord.gg/spyro)<br/>

## A custom Discord .NET bot programmed for the subreddit's guild

### Current Features
- Community vote to pin message
- Custom randomized welcome messages
- Basic automated role request

### TODO
- Add More Per Server Variables
- Periodic Messages (Timer or Per Message Count)
- More Vote Options
- Random User Selection (Giveaways, or you just want to pick on someone random)
